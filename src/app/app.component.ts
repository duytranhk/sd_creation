import { Component, OnInit } from '@angular/core';
import { ApplicationEnvModel, EnvironmentConstModel, ApplicationConst, BranchConstModel } from './constants/application.const';
import { AuthModel } from './models/auth.model';
import { ApiEndpointService } from './services/api-endpoint.service';
import { StorageKey } from './constants/storage-key.const';
import { StorageManagerService } from './services/storage-manager.service';
import { IApiResult, DuplicateResponse } from './models/common.model';
import { MatSnackBar } from '@angular/material/snack-bar';

interface SdCreationModel {
  EnvId?: number;
  BranchId?: number;
  SelectedEnv?: ApplicationEnvModel;
  SelectedBranch?: BranchConstModel;
  SelectedPersonasId?: number;
}
interface ClientModel {
  FirstName?: string;
  LastName?: string;
  Email?: string;
  UserName?: string;
  ActivationLink?: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  public environmentList: EnvironmentConstModel[] = ApplicationConst.ENV_LIST;
  public branchLists: BranchConstModel[] = [];
  public vm: SdCreationModel = {};
  public client: ClientModel = {};
  public loading = false;
  constructor(
    private apiEndpointService: ApiEndpointService,
    private storageManager: StorageManagerService,
    private snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
    const self = this;
  }
  onSelectEnvironment(): void {
    const self = this;
    self.vm.SelectedEnv = ApplicationConst.SD_ENV.find(e => e.Id === Number(this.vm.EnvId));
  }
  onSelectBranch(): void {
    const self = this;
    self.loading = true;
    self.vm.SelectedBranch = self.vm.SelectedEnv.Branches.find(b => b.Id === Number(self.vm.BranchId));
    self.apiEndpointService.callApi<AuthModel>('POST', self.vm.SelectedBranch.ApiUrl + 'api/Account/auth', {
      UserName: self.vm.SelectedBranch.AgentEmail,
      Password: self.vm.SelectedBranch.AgentPassword
    }).subscribe(data => {
      self.storageManager.localStorageSetItem(StorageKey.ACCESS_TOKEN, data.access_token);
      self.storageManager.localStorageSetItem(StorageKey.TOKEN_TYPE, data.token_type);
      self.apiEndpointService.callApi<IApiResult<any>>('GET', self.vm.SelectedBranch.ApiUrl + '/api/personas/GetAgentPersonas')
        .subscribe(resp => {
          if (resp.Results) {
            self.vm.SelectedPersonasId = resp.Results.Personas[0].Id;
          }
          self.loading = false;
        }, err => {
          self.showMessage('Fail to fetch default personas');
          self.loading = false;
        });
    }, err => {
      self.showMessage('Authorization Fail');
      self.loading = false;
    });
  }
  onClickCreate(): void {
    const self = this;
    self.loading = true;
    self.client.ActivationLink = '';
    self.apiEndpointService.callApi<IApiResult<DuplicateResponse>>('POST', self.vm.SelectedBranch.ApiUrl + 'api/personas/Duplicate', {
      PersonaId: self.vm.SelectedPersonasId,
      FirstName: self.client.FirstName,
      LastName: self.client.LastName
    }).subscribe(resp => {
      if (resp.Success) {
        const customerId = resp.Results.CustomerId;
        self.apiEndpointService.callApi<IApiResult<string>>('POST', self.vm.SelectedBranch.ApiUrl + 'api/Account/Create', {
          CustomerId: customerId,
          Email: self.client.Email,
          UserName: self.client.UserName
        }).subscribe(activationResponse => {
          if (activationResponse.Success) {
            self.client.ActivationLink = activationResponse.Results;
          } else {
            self.showMessage(activationResponse.ErrorMessage);
          }
          self.loading = false;
        }, err => {
          self.showMessage('Fail to create account, double check your email and try again');
          self.loading = false;
        });
      }
    }, err => {
      self.showMessage('Could not dupplicate personas');
      self.loading = false;
    });
  }
  onClickReset(): void {
    const self = this;
    self.client.FirstName = '';
    self.client.LastName = '';
    self.client.Email = '';
    self.client.UserName = '';
  }
  showMessage(message: string): void {
    const self = this;
    self.snackBar.open(message, 'dismiss', {
      duration: 3500,
    });
  }
}
