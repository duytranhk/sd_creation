import { Injectable } from '@angular/core';
export interface BranchConstModel {
    Id: number;
    BranchName: string;
    AgentEmail: string;
    AgentPassword: string;
    ApiUrl: string;
}
export interface ApplicationEnvModel {
    Id: number;
    Branches: BranchConstModel[];
}
export interface EnvironmentConstModel {
    Id: number;
    Name: string;
    Code: string;
    IsEnable: boolean;
}
@Injectable()
export class ApplicationConst {
    public static ENV_LIST: EnvironmentConstModel[] = [
        {
            Id: 0,
            Name: 'Develop',
            Code: 'CI',
            IsEnable: true,
        },
        {
            Id: 1,
            Name: 'Release',
            Code: 'RC',
            IsEnable: false,
        },
        {
            Id: 2,
            Name: 'Preprod',
            Code: 'PREPROD',
            IsEnable: true,
        },
        {
            Id: 3,
            Name: 'Prod',
            Code: 'PROD',
            IsEnable: false
        }
    ];
    public static SD_ENV: ApplicationEnvModel[] = [
        {
            Id: 0,
            Branches: [
                {
                    Id: 102,
                    ApiUrl: 'https://app.btotest.com/',
                    BranchName: 'SD Default Singapore',
                    AgentEmail: 'sd_agent@sgp.com',
                    AgentPassword: '1qazXSW@'
                },
                {
                    Id: 103,
                    ApiUrl: 'https://are.btotest.com/',
                    BranchName: 'SD Default UAE',
                    AgentEmail: 'sd_agent@uae.com',
                    AgentPassword: '1qazXSW@'
                },
                {
                    Id: 104,
                    ApiUrl: 'https://zur.btotest.com/',
                    BranchName: 'SD Zurich',
                    AgentEmail: 'sd_agent@zur.com',
                    AgentPassword: '1qazXSW@'
                },
                {
                    Id: 105,
                    ApiUrl: 'https://scb.btotest.com/',
                    BranchName: 'SD SCB Singapore',
                    AgentEmail: 'sd_agent@scb.com',
                    AgentPassword: '1qazXSW@'
                }
            ]
        },
        {
            Id: 2,
            Branches: [
                {
                    ApiUrl: 'https://slf.btoprod.com/',
                    Id: 40,
                    BranchName: 'Sunlife Default',
                    AgentEmail: 'agent_slf001@yopmail.com',
                    AgentPassword: 'Passw0rd!'
                }
            ]
        }
    ];
}
