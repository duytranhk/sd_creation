import { Injectable } from '@angular/core';

@Injectable()
export class StorageKey {
  public static readonly ACCESS_TOKEN = 'access_token';
  public static readonly TOKEN_TYPE = 'token_type';
}
