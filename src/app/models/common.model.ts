export interface IApiResult<T> {
    ErrorCode: number;
    ErrorMessage: string;
    ErrorSource: string;
    Results: T;
    Success: boolean;
}

export interface DuplicateResponse {
    CustomerId: number;
    FirstName: string;
    HouseholdId: number;
    LastName: string;
    PlanId: number;
}