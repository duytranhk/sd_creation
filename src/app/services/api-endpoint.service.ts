import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageManagerService } from './storage-manager.service';
import { StorageKey } from '../constants/storage-key.const';

@Injectable()
export class ApiEndpointService {
    constructor(private http: HttpClient, private storageManager: StorageManagerService) {
    }
    public callApi<T>(method: string, url: string, data?: any) {
        const self = this;
        const tokenType = self.storageManager.localStorageGetItem(StorageKey.TOKEN_TYPE);
        const token = self.storageManager.localStorageGetItem(StorageKey.ACCESS_TOKEN);
        return self.http.request<T>(method, url, {
            body: data,
            headers: token && tokenType ? new HttpHeaders({
                Authorization: `${tokenType} ${token}`
            }) : null
        });
    }
}
